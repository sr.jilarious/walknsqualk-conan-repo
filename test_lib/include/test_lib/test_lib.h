#pragma once

#ifdef WIN32
  #define test_lib_EXPORT __declspec(dllexport)
#else
  #define test_lib_EXPORT
#endif

test_lib_EXPORT void test_lib();
