#include <iostream>
#include <test_lib/test_lib.h>

void test_lib(){
    #ifdef NDEBUG
    std::cout << "test_lib/0.1: Hello World Release!" <<std::endl;
    #else
    std::cout << "test_lib/0.1: Hello World Debug!" <<std::endl;
    #endif
}
